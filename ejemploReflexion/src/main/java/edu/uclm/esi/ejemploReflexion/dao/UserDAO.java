package edu.uclm.esi.ejemploReflexion.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.uclm.esi.ejemploReflexion.model.*;

public class UserDAO {

	public static void insert(User user, String pwd) throws Exception {
		Connection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into users (email, user_name, pwd) values (?, ?, ?)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, user.getEmail());
			ps.setString(2, user.getUserName());
			ps.setString(3, pwd);
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static User identify(String userName, String pwd) throws Exception {
		Connection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select email from user where user_name=? and pwd=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, userName);
			ps.setString(2, pwd);
			ResultSet rs=ps.executeQuery();
			if (rs.next()) {
				User user=new User();
				user.setEmail(rs.getString(1));
				user.setUserName(userName);
				return user;
			}
			throw new Exception("Credenciales inválidas");
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}
}

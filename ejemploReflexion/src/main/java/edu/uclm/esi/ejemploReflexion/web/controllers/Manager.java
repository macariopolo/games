package edu.uclm.esi.ejemploReflexion.web.controllers;

import edu.uclm.esi.ejemploReflexion.model.*;

public class Manager {
	
	private Manager() {
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}


}

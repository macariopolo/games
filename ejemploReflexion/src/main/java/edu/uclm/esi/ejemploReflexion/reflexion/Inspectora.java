package edu.uclm.esi.ejemploReflexion.reflexion;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Inspectora {
	private Object object;

	public Inspectora(Object object) {
		this.object=object;
	}
	
	public void show() {
		Class<?> clase=object.getClass();
		System.out.println("Inspección de la clase: " + clase.getName());
		showFields(clase);
		showMethods(clase);
	}

	private void showMethods(Class<?> clase) {
		Method[] methods=clase.getDeclaredMethods();
		Method method;
		for (int i=0; i<methods.length; i++) {
			method=methods[i];
			method.setAccessible(true);
			System.out.println("\t" + method.getName() + " ... " + method.getReturnType().getName());
		}
	}

	private void showFields(Class<?> clase) {
		Field[] fields=clase.getDeclaredFields();
		Field field;
		for (int i=0; i<fields.length; i++) {
			field=fields[i];
			field.setAccessible(true);
			System.out.println("\t" + field.getName() + " ... " + field.getType().getName());
		}
	}
	
	public JSONObject toJSON() throws JSONException {
		Class<?> clase=this.object.getClass();
		JSONObject jso=new JSONObject();
		jso.put("nombre", clase.getName());
		JSONArray jsaFields=new JSONArray();
		Field[] fields=clase.getDeclaredFields();
		Field field;
		for (int i=0; i<fields.length; i++) {
			field=fields[i];
			JSONObject jsoField = new JSONObject();
			jsoField.put("nombre", field.getName());
			jsoField.put("tipo", field.getType().getName());
			jsaFields.put(jsoField);
		}
		jso.put("campos", jsaFields);
		return jso;
	}
}

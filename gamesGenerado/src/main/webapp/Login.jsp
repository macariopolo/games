<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.*, edu.uclm.esi.gamesGenerado.web.controllers.Manager" %>

<%
String p=request.getParameter("p");
JSONObject resultado=new JSONObject();
try {
	if (!request.getMethod().equals("POST"))
		throw new Exception("Método no soportado");
	JSONObject jso=new JSONObject(p);
	
	if (!jso.getString("tipo").equals("Login")) {
		resultado.put("tipo", "error");
		resultado.put("texto", "Mensaje inesperado");
	} else {
		String userName=jso.getString("userName");
		String pwd=jso.getString("pwd");
		JSONObject jsoRespuesta=Manager.get().login(userName, pwd);
		resultado.put("resultado", jsoRespuesta);
		resultado.put("tipo", "OK");
	}
}
catch (Exception e) {
	resultado.put("tipo", "error");
	resultado.put("texto", e.getMessage());
}
%>

<%= resultado.toString() %>

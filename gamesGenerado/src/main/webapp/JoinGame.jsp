<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.*, edu.uclm.esi.gamesGenerado.web.controllers.Manager" %>

<%
String p=request.getParameter("p");
JSONObject resultado=new JSONObject();
try {
	if (!request.getMethod().equals("POST"))
		throw new Exception("Método no soportado");
	JSONObject jso=new JSONObject(p);
	
	if (!jso.getString("tipo").equals("JoinGame")) {
		resultado.put("tipo", "error");
		resultado.put("texto", "Mensaje inesperado");
	} else {
		String gameName=jso.getString("gameName");
		JSONObject jsoRespuesta=Manager.get().joinGame(gameName);
		resultado.put("resultado", jsoRespuesta);
		resultado.put("tipo", "OK");
	}
}
catch (Exception e) {
	resultado.put("tipo", "error");
	resultado.put("texto", e.getMessage());
}
%>

<%= resultado.toString() %>

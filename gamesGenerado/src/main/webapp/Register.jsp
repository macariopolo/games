<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.*, edu.uclm.esi.gamesGenerado.web.controllers.Manager" %>

<%
String p=request.getParameter("p");
JSONObject resultado=new JSONObject();
try {
	if (!request.getMethod().equals("POST"))
		throw new Exception("Método no soportado");
	JSONObject jso=new JSONObject(p);
	
	if (!jso.getString("tipo").equals("Register")) {
		resultado.put("tipo", "error");
		resultado.put("texto", "Mensaje inesperado");
	} else {
		String email=jso.getString("email");
		String userName=jso.getString("userName");
		String pwd1=jso.getString("pwd1");
		String pwd2=jso.getString("pwd2");
		JSONObject jsoRespuesta=Manager.get().register(email, userName, pwd1, pwd2);
		resultado.put("resultado", jsoRespuesta);
		resultado.put("tipo", "OK");
	}
}
catch (Exception e) {
	resultado.put("tipo", "error");
	resultado.put("texto", e.getMessage());
}
%>

<%= resultado.toString() %>

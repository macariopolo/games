package edu.uclm.esi.gamesGenerado.json;

import java.io.Serializable;
import java.lang.reflect.Field;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class AbstractJSONMessage implements Serializable {	
	protected int idMessage=new java.util.Random().nextInt(Integer.MAX_VALUE);

	public AbstractJSONMessage(JSONObject jso) {
		Class<AbstractJSONMessage> clazz=(Class<AbstractJSONMessage>) this.getClass();
		Field ff []=clazz.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			try {
				field.set(this, jso.get(field.getName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setIdMessage(int idMessage) {
		this.idMessage = idMessage;
	}
	
	public int getIdMessage() {
		return idMessage;
	}
	
	@Override
	public String toString() {
		String type=this.getClass().getSimpleName().substring(4);
		JSONObject jso=new JSONObject();
		try {
			jso.put("TYPE", type);
			jso.put("idMessage", idMessage);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		Class<AbstractJSONMessage> clazz=(Class<AbstractJSONMessage>) this.getClass();
		Field ff []=clazz.getDeclaredFields();
		for (Field field : ff) {
			field.setAccessible(true);
			try {
				jso.put(field.getName(), field.get(this));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		return jso.toString();
	}
}



package edu.uclm.esi.gamesGenerado.json;

import org.json.*;

public class Movement extends AbstractJSONMessage {
	private Integer idPartida;
	

	public Movement(JSONObject jso) {
		super(jso);
	}
	
	public void setIdPartida(Integer idPartida) {
		this.idPartida=idPartida;
	}
	
}

package edu.uclm.esi.gamesGenerado.model;

import java.util.*;
import java.util.concurrent.*;

import org.json.*;

public  class Player   {
	private String email;
	private String user_name;
	private Match match;

	public Player() {
		super();
		
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public void setMatch(Match match) {
		this.match = match;
	}

	public String getEmail() {
		return this.email;
	}
	
	public String getUser_name() {
		return this.user_name;
	}
	
	public Match getMatch() { // a
		return this.match;
	}
}

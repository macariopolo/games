package edu.uclm.esi.gamesGenerado.web.ws;

import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.uclm.esi.gamesGenerado.model.*;
import edu.uclm.esi.gamesGenerado.json.*;
import edu.uclm.esi.gamesGenerado.web.controllers.Manager;

@Component
public class WSMove extends TextWebSocketHandler {
	private static ConcurrentHashMap<String, WebSocketSession> sessionsById=new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, WebSocketSession> sessionsByUser=new ConcurrentHashMap<>();
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessionsById.put(session.getId(), session);
		User user = (User) session.getAttributes().get("user");
		String userName=user.getUserName();
		if (sessionsByUser.get(userName)!=null) 
			sessionsByUser.remove(userName);
		sessionsByUser.put(userName, session);
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		System.out.println(message.getPayload());
		JSONObject jso=new JSONObject(message.getPayload());
		if (jso.getString("TYPE").equals("Movement")) {
			JSONObject jsoMovement = jso.getJSONObject("Movement");
			Movement movement = new Movement(jsoMovement);
			return;
		}
		if (jso.getString("TYPE").equals("Retreat")) {
			JSONObject jsoRetreat = jso.getJSONObject("Retreat");
			Retreat retreat = new Retreat(jsoRetreat);
			return;
		}
	}

	private void sendError(WebSocketSession session, String message) throws Exception {
		JSONObject jso = new JSONObject();
		jso.put("TYPE", "ERROR");
		jso.put("MESSAGE", message);
		WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
		session.sendMessage(wsMessage);
	}
}

package edu.uclm.esi.gamesGenerado.model;

import java.util.*;
import java.util.concurrent.*;

import org.json.*;

public abstract class Match   {
	private Integer id;
	private Player player; 
	private Player playerWithTurn;

	public Match() {
		super();
		
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	} 
	
	public void setPlayerWithTurn(Player playerWithTurn) {
		this.playerWithTurn = playerWithTurn;
	}

	public Integer getId() {
		return this.id;
	}
	
	public Player getPlayer() { // a
		return this.player;
	} 
	
	public Player getPlayerWithTurn() { // a
		return this.playerWithTurn;
	}
}

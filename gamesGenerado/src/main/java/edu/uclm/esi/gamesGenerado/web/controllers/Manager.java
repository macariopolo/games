package edu.uclm.esi.gamesGenerado.web.controllers;

import edu.uclm.esi.gamesGenerado.model.*;

import org.json.*;

import java.util.concurrent.*;
import java.util.Vector;

public class Manager {
	private Player player; 
	private ConcurrentHashMap<String, Game> games;
	
	private Manager() {
		this.games = new ConcurrentHashMap<String, Game>();
	}

	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}
	
	public JSONObject register(String email, String userName, String pwd1, String pwd2) {
		JSONObject result=new JSONObject();
		return result;
	}
	
	public JSONObject joinGame(String gameName) {
		JSONObject result=new JSONObject();
		return result;
	}
	
	public JSONObject listaDeJuegos() {
		JSONObject result=new JSONObject();
		return result;
	}
	
	public JSONObject login(String userName, String pwd) {
		JSONObject result=new JSONObject();
		return result;
	}
	

	public Player getPlayer() { // a
		return this.player;
	} 
	
	public ConcurrentHashMap<String, Game> getGames() { // c
		return this.games;
	}

	public void setPlayer(Player player) {
		this.player = player;
	} 
	
	public void addToGames(Game game) { // 1
		this.games.put(game.getName(), game);
	}
}

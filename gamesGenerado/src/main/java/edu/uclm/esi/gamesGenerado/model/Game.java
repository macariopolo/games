package edu.uclm.esi.gamesGenerado.model;

import java.util.*;
import java.util.concurrent.*;

import org.json.*;

public abstract class Game   {
	private String name;
	private ConcurrentHashMap<Integer, Match> inPlayMatches;
	private Vector<Match> pendingMatches;

	public Game() {
		super();
		this.inPlayMatches = new ConcurrentHashMap<Integer, Match>();
		this.pendingMatches = new Vector<Match>();
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addToInPlayMatches(Match match) { // 1
		this.inPlayMatches.put(match.getId(), match);
	}
	
	public void addToPendingMatches(Match match) { // 2
		this.pendingMatches.add(match); 
	}

	public String getName() {
		return this.name;
	}
	
	public ConcurrentHashMap<Integer, Match> getInPlayMatches() { // c
		return this.inPlayMatches;
	}
	
	public Vector<Match> getPendingMatches() { // d
		return this.pendingMatches;
	}
}

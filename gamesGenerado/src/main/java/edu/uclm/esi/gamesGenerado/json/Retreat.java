package edu.uclm.esi.gamesGenerado.json;

import org.json.*;

public class Retreat extends AbstractJSONMessage {
	private Integer idPartida;
	

	public Retreat(JSONObject jso) {
		super(jso);
	}
	
	public void setIdPartida(Integer idPartida) {
		this.idPartida=idPartida;
	}
	
}

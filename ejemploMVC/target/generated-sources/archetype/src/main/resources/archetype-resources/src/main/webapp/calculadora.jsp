#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<%@ page language="java" contentType="text/plain; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	int a=Integer.parseInt(request.getParameter("a"));
	int b=Integer.parseInt(request.getParameter("b"));
	char operador=request.getParameter("operador").charAt(0);
	
	if (operador=='+')
		out.print(a+b);
	else if (operador=='-')
		out.print(a-b);
	else if (operador=='*')
		out.print(a*b);
	else
		out.print(a/b);
%>
package edu.uclm.esi.tests;

import java.util.regex.Pattern;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import edu.uclm.esi.games.dao.Broker;
import edu.uclm.esi.games.dao.PooledConnection;

public class TestRecuperarPwd {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "/Users/macario.polo/Desktop/chromedriver");
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    try {
		  String sql="delete from token";
		  PooledConnection bd = Broker.get().getBd();
		  PreparedStatement ps = bd.prepareStatement(sql);
		  ps.executeUpdate();
		  bd.close();
	  }
	  catch (Exception e) {
		fail("No esperaba excepción");
	}
  }

  @Test
  public void testUntitledTestCase() throws Exception {
    driver.get("https://www.google.com");
    driver.get("http://10.30.7.54:8080/");
    driver.findElement(By.linkText("Login")).click();
    driver.findElement(By.id("loginUserName")).click();
    driver.findElement(By.id("loginUserName")).clear();
    driver.findElement(By.id("loginUserName")).sendKeys("pepe");
    driver.findElement(By.id("btnRecuperarPwd")).click();
    driver.findElement(By.linkText("Haz clic aquí")).click();
    driver.findElement(By.id("userName")).click();
    driver.findElement(By.id("userName")).clear();
    driver.findElement(By.id("userName")).sendKeys("pepe");
    driver.findElement(By.id("pwd1")).clear();
    driver.findElement(By.id("pwd1")).sendKeys("pepe123");
    driver.findElement(By.id("pwd2")).clear();
    driver.findElement(By.id("pwd2")).sendKeys("pepe456");
    driver.findElement(By.xpath("//body")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Recuperación de password'])[2]/following::button[1]")).click();
    Thread.sleep(500);
    assertEquals("Error al cambiar la password", closeAlertAndGetItsText()); // Comprobacion de cliente
    
    try { // Comprobacion de servidor
		  String sql="select count(*) from token";
		  PooledConnection bd = Broker.get().getBd();
		  PreparedStatement ps = bd.prepareStatement(sql);
		  ResultSet rs=ps.executeQuery();
		  rs.next();
		  assertTrue(rs.getInt(1)==0);
		  bd.close();
	  }
	  catch (Exception e) {
		fail("No esperaba excepción");
	}
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

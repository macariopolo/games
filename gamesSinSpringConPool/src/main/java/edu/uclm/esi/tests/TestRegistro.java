package edu.uclm.esi.tests;

import java.util.regex.Pattern;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import edu.uclm.esi.games.dao.Broker;
import edu.uclm.esi.games.dao.PooledConnection;

public class TestRegistro {
  private WebDriver driverLucas;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  
  public TestRegistro() {
	System.setProperty("webdriver.chrome.driver", "/Users/macario.polo/Desktop/chromedriver");
	System.setProperty("webdriver.gecko.driver", "/Users/macario.polo/Desktop/geckodriver");
  }

  @Before
  public void setUp() throws Exception {
    driverLucas = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driverLucas.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driverLucas.manage().window().setPosition(new Point(0, 0));
    try {
		  String sql="delete from player";
		  PooledConnection bd = Broker.get().getBd();
		  PreparedStatement ps = bd.prepareStatement(sql);
		  ps.executeUpdate();
		  bd.close();
	  }
	  catch (Exception e) {
		fail("No esperaba excepción");
	}
  }

  @Test
  public void testRegistroCorrecto() throws Exception {
	  driverLucas.get("http://localhost:8080/index.html");
	  driverLucas.findElement(By.linkText("Registro")).click();
	  
	  WebElement we=driverLucas.findElement(By.id("registroEmail"));
	  we.click(); we.clear(); we.sendKeys("lucas@lucas.com");
	  
	  we=driverLucas.findElement(By.id("registroUserName"));
	  we.click(); we.clear(); we.sendKeys("lucas");
	  
	  we=driverLucas.findElement(By.id("registroPwd1"));
	  we.click(); we.clear(); we.sendKeys("lucas");
	  
	  we=driverLucas.findElement(By.id("registroPwd2"));
	  we.click(); we.clear(); we.sendKeys("lucas");
	  
	  we=driverLucas.findElement(By.id("registroBtn"));
	  we.click(); 
	  
	  Thread.sleep(500);
	  String textoQueTieneQueEstar="Registrado correctamente";	  
	  we=driverLucas.findElement(By.id("mensajeCorto"));
	  assertTrue(we.getText().contains(textoQueTieneQueEstar));
	  
	  try {
		  String sql="select count(*) from player where user_name='lucas'";
		  PooledConnection bd = Broker.get().getBd();
		  PreparedStatement ps=bd.prepareStatement(sql);
		  ResultSet rs=ps.executeQuery();
		  rs.next();
		  int n=rs.getInt(1);
		  bd.close();
		  assertTrue(n==1);
	  }
	  catch (Exception e) {
		fail("No esperaba excepción");
	}
  }

  @After
  public void tearDown() throws Exception {
    driverLucas.quit();
  }

  private boolean isElementPresent(By by) {
    try {
      driverLucas.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driverLucas.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driverLucas.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

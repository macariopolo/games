package edu.uclm.esi.games.dao;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Broker {
	private ConcurrentLinkedQueue<PooledConnection> libres, ocupadas;
	
	private Broker() {
		try {
			this.libres=new ConcurrentLinkedQueue<>();
			this.ocupadas=new ConcurrentLinkedQueue<>();
			
			Class.forName("com.mysql.jdbc.Driver");
			PooledConnection.url="jdbc:mysql://alarcosj.esi.uclm.es:3306/ideasiberdrola?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false";
			PooledConnection.user="ideas";
			PooledConnection.pwd="ideas123";
			
			for (int i=1; i<20; i++) {
				PooledConnection pooledConnection=new PooledConnection(this);
				this.libres.offer(pooledConnection);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	private static class BrokerHolder {
		static Broker singleton=new Broker();
	}
	
	public static Broker get() {
		return BrokerHolder.singleton;
	}

	public PooledConnection getBd() throws SQLException {
		PooledConnection pooledConnection=this.libres.poll();
		this.ocupadas.offer(pooledConnection);
		return pooledConnection;
	}

	public void move(PooledConnection pooledConnection) {
		this.ocupadas.remove(pooledConnection);
		this.libres.offer(pooledConnection);
	}
}

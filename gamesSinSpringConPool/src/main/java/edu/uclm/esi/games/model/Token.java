package edu.uclm.esi.games.model;

import java.util.Random;

import edu.uclm.esi.games.dao.TokenDAO;

public class Token {
	private int id;
	private long validTime;
	private Player player; 
	
	public Token(Player player) {
		this.player=player;
		this.id=Math.abs(new Random().nextInt());
		this.validTime=System.currentTimeMillis() + 10*60*1000;
	}
	
	public void insert() throws Exception {
		TokenDAO.insert(this);
	}
	
	public int getId() {
		return id;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public long getValidTime() {
		return validTime;
	}

	public static void load(String userName, int idToken) throws Exception {
		TokenDAO.find(userName, idToken);
	}

	public static void delete(int id) throws Exception {
		TokenDAO.delete(id);
	}
}

package edu.uclm.esi.games.model;

import java.util.UUID;

public abstract class Match {
	protected UUID id;
	protected Player playerA, playerB;
	protected int currentPlayer;
	protected Player winner;
	protected Board board;
	private Game game;
	
	public Match(Game game) {
		this.game=game;
		this.id=UUID.randomUUID();
		this.currentPlayer=-1;
	}
	
	public UUID getId() {
		return id;
	}

	public void addPlayer(Player player) {
		if (this.playerA==null)
			this.playerA=player;
		else 
			this.playerB=player;
		player.setCurrentMatch(this);
	}
	
	public Player getPlayerA() {
		return playerA;
	}

	public Player getPlayerB() {
		return playerB;
	}

	public Board getBoard() {
		return board;
	}
	
	public Player getWinner() {
		return winner;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	
	public String getCurrentPlayerUserName() {
		if (!isComplete())
			return null;
		return currentPlayer==0 ? this.playerA.getUserName() : this.playerB.getUserName();
	}
	
	public String getGameName() {
		return game.getName();
	}

	public Match move(Player player, int[] coordinates) throws Exception {
		if (!tieneElTurno(player))
			throw new Exception("No tienes el turno");
		if (this.board.end())
			throw new Exception("La partida ha terminado");
		this.board.move(player, coordinates);
		this.winner=this.board.getWinner();
		if (winner!=null) {
			this.playerA.setCurrentMatch(null);
			this.playerB.setCurrentMatch(null);
			this.game.inPlayMatches.remove(this.getId());
		}
		this.currentPlayer=(this.currentPlayer+1) % 2;
		return this;
	}


	protected abstract boolean tieneElTurno(Player player);

	public abstract void calculateFirstPlayer();

	public void setWinner(Player player) {
		this.winner=player;
	}

	public boolean isComplete() {
		return this.playerA!=null && this.playerB!=null;
	}

}

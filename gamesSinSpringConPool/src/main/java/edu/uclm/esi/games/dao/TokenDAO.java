package edu.uclm.esi.games.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.uclm.esi.games.model.Player;
import edu.uclm.esi.games.model.Token;

public class TokenDAO {

	public static void insert(Token token) throws Exception {
		PooledConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into token (id, user_name, valid_time) values (?, ?, ?)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setInt(1, token.getId());
			ps.setString(2, token.getPlayer().getUserName());
			ps.setLong(3, token.getValidTime());
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void find(String userName, int idToken) throws Exception {
		PooledConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select valid_time from token where user_name=? and id=? order by valid_time desc limit 1";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, userName);
			ps.setInt(2, idToken);
			ResultSet rs=ps.executeQuery();
			if (rs.next()) {
				long validTime=rs.getLong(1);
				if (validTime<System.currentTimeMillis())
					throw new Exception("Token expirado o token no existe");
			}
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static void delete(int id) throws Exception {
		PooledConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="delete from token where id=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

}

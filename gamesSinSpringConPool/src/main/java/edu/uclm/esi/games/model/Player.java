package edu.uclm.esi.games.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.uclm.esi.games.dao.PlayerDAO;

public class Player {
	private String userName;
	private String email;
	@JsonIgnore 
	private Match currentMatch;
	
	public Player() {
	}
		
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setCurrentMatch(Match match) {
		this.currentMatch=match;
	}
	
	public Match getCurrentMatch() {
		return currentMatch;
	}
	
	public Match move(int[] coordinates) throws Exception {
		return this.currentMatch.move(this, coordinates);
	}

	public static Player insert(String email, String userName, String pwd) throws Exception {
		Player player=new Player();
		player.setEmail(email);
		player.setUserName(userName);
		PlayerDAO.insert(player, pwd);
		return player;
	}

	public static Player identify(String userName, String pwd) throws Exception {
		return PlayerDAO.identify(userName, pwd);
	}

	public static Player find(String userName) throws Exception {
		return PlayerDAO.find(userName);
	}

	public static Player updatePwd(String userName, String pwd) throws Exception {
		return PlayerDAO.updatePwd(userName, pwd);
	}

}

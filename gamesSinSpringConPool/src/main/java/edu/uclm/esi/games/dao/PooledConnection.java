package edu.uclm.esi.games.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PooledConnection {

	public static String url;
	public static String user;
	public static String pwd;
	private Broker broker;
	private Connection connection;
	
	public PooledConnection(Broker broker) throws SQLException {
		this.broker=broker;
		this.connection=DriverManager.getConnection(url, user, pwd);
	}

	public void close() {
		this.broker.move(this);
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return this.connection.prepareStatement(sql);
	}
}

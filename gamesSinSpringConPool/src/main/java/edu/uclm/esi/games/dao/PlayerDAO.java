package edu.uclm.esi.games.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import edu.uclm.esi.games.model.Player;

public class PlayerDAO {

	public static void insert(Player player, String pwd) throws Exception {
		PooledConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="insert into player (email, user_name, pwd, activo) values (?, ?, ?, true)";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, player.getEmail());
			ps.setString(2, player.getUserName());
			ps.setString(3, pwd);
			ps.executeUpdate();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static Player identify(String userName, String pwd) throws Exception {
		PooledConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select email from player where user_name=? and pwd=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, userName);
			ps.setString(2, pwd);
			ResultSet rs=ps.executeQuery();
			if (rs.next()) {
				Player player=new Player();
				player.setEmail(rs.getString(1));
				player.setUserName(userName);
				return player;
			}
			throw new Exception("Credenciales inválidas");
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static Player find(String userName) throws Exception {
		PooledConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select email from player where user_name=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, userName);
			ResultSet rs=ps.executeQuery();
			if (rs.next()) {
				Player player=new Player();
				player.setEmail(rs.getString(1));
				player.setUserName(userName);
				return player;
			}
			return null;
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

	public static Player updatePwd(String userName, String pwd) throws Exception {
		PooledConnection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="update player set pwd=? where user_name=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, pwd);
			ps.setString(2, userName);
			ps.executeUpdate();
			return PlayerDAO.identify(userName, pwd);
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}

}

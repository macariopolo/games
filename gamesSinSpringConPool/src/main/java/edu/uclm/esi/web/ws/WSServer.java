package edu.uclm.esi.web.ws;

import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.uclm.esi.games.model.Match;
import edu.uclm.esi.games.model.Player;
import edu.uclm.esi.web.Manager;

@Component
public class WSServer extends TextWebSocketHandler {
	private static ConcurrentHashMap<String, WebSocketSession> sessionsById=new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, WebSocketSession> sessionsByPlayer=new ConcurrentHashMap<>();
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessionsById.put(session.getId(), session);
		Player player = (Player) session.getAttributes().get("player");
		String userName=player.getUserName();
		if (sessionsByPlayer.get(userName)!=null) 
			sessionsByPlayer.remove(userName);
		sessionsByPlayer.put(userName, session);
		System.out.println(userName);
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		System.out.println(message.getPayload());
		JSONObject jso=new JSONObject(message.getPayload());
		if (jso.getString("TYPE").equals("MOVEMENT")) {
			Player player = (Player) session.getAttributes().get("player");
			JSONArray coordinates = jso.getJSONArray("coordinates");
			try {
				Match match=Manager.get().move(player, coordinates);
				sendMovement(match);
			}
			catch (Exception e) {
				sendError(player, e.getMessage());
			}
		}
	}

	private void sendError(Player player, String message) throws Exception {
		WebSocketSession session=sessionsByPlayer.get(player.getUserName());
		JSONObject jso = new JSONObject();
		jso.put("TYPE", "ERROR");
		jso.put("MESSAGE", message);
		WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
		session.sendMessage(wsMessage);
	}

	public static void sendNewMatch(Match match) {
		try {
			sendNewMatch(match.getPlayerA(), match);
			sendNewMatch(match.getPlayerB(), match);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	private static void sendNewMatch(Player player, Match match) throws Exception {
		ObjectMapper mapper=new ObjectMapper();		
		JSONObject jso = new JSONObject(mapper.writeValueAsString(match));
		jso.put("TYPE", "NEW_MATCH");
		WebSocketSession session=sessionsByPlayer.get(player.getUserName());
		WebSocketMessage<?> message=new TextMessage(jso.toString());
		session.sendMessage(message);
	}
	
	private static void sendMovement(Match match) throws Exception {
		try {
			sendMovement(match.getPlayerA(), match);
			sendMovement(match.getPlayerB(), match);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private static void sendMovement(Player player, Match match) throws Exception {
		ObjectMapper mapper=new ObjectMapper();		
		JSONObject jso = new JSONObject(mapper.writeValueAsString(match));
		jso.put("TYPE", "MOVEMENT");
		WebSocketSession session=sessionsByPlayer.get(player.getUserName());
		WebSocketMessage<?> message=new TextMessage(jso.toString());
		session.sendMessage(message);
	}
}

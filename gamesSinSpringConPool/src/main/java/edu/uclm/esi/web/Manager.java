package edu.uclm.esi.web;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;

import edu.uclm.esi.games.model.Game;
import edu.uclm.esi.games.model.Match;
import edu.uclm.esi.games.model.Player;
import edu.uclm.esi.games.model.Token;
import edu.uclm.esi.games.ppt.PPTGame;
import edu.uclm.esi.games.tictactoe.TictactoeGame;

public class Manager {
	private ConcurrentHashMap<String, Game> games;
	private ConcurrentHashMap<String, Player> players;
	
	private Manager() {
		games=new ConcurrentHashMap<>();
		Game tictactoe=new TictactoeGame();
		games.put(tictactoe.getName(), tictactoe);
		Game ppt=new PPTGame();
		games.put(ppt.getName(), ppt);
		
		this.players=new ConcurrentHashMap<>();
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}

	public Match joinGame(Player player, String gameName) {
		Game game=this.games.get(gameName);
		return game.getMatch(player);
	}

	public JSONArray getGames() {
		JSONArray jsa=new JSONArray();
		Enumeration<Game> eGames = games.elements();
		while (eGames.hasMoreElements())
			jsa.put(eGames.nextElement().getName());
		return jsa;
	}

	public Match move(Player player, JSONArray coordinates) throws Exception {
		int[] iC=new int[coordinates.length()];
		for (int i=0; i<iC.length; i++)
			iC[i]=coordinates.getInt(i);
		Match match=player.move(iC);
		return match;
	}

	public void logout(Player player) {
		// TODO Auto-generated method stub
		
	}

	public void addPlayer(Player player) {
		this.players.put(player.getUserName(), player);
	}

	public Player getPlayer(String userName) {
		return this.players.get(userName);
	}

	public void remove(String userName) {
		this.players.remove(userName);
	}

	public Token requestToken(Player player) throws Exception {
		Token token=new Token(player);
		token.insert();
		return token;
	}

	public Player resetPwd(String userName, String pwd, int idToken) throws Exception {
		Token.load(userName, idToken);
		return Player.updatePwd(userName, pwd);
	}

}

package edu.uclm.esi.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import edu.uclm.esi.games.model.Match;
import edu.uclm.esi.games.model.Player;
import edu.uclm.esi.games.model.Token;
import edu.uclm.esi.web.exceptions.LoginException;

@RestController
public class WebController {
	@RequestMapping("/register")
	public Player register(@RequestParam(value="email") String email, @RequestParam(value="userName") String userName, @RequestParam(value="pwd1") String pwd1, @RequestParam(value="pwd2") String pwd2) throws Exception {
		if (!pwd1.equals(pwd2))
			throw new Exception("Error: las contraseñas no coinciden");
		Player player=Player.insert(email, userName, pwd1);
		return player;
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public Player login(HttpSession session, @RequestParam(value="userName") String userName, @RequestParam(value="pwd") String pwd) throws Exception {
		Player player=Player.identify(userName, pwd);

		Player oldPlayer=Manager.get().getPlayer(userName);
		if (oldPlayer!=null) {
			Manager.get().remove(oldPlayer.getUserName());
			player.setCurrentMatch(oldPlayer.getCurrentMatch());
		}
		
		Manager.get().addPlayer(player);
		session.setAttribute("player", player);
		return player;
	}
	
	@RequestMapping(value="/joinGame", method=RequestMethod.POST)
	public Match joinGame(HttpSession session, @RequestParam(value="gameName") String gameName) throws Exception {
		Player player=(Player) session.getAttribute("player");
		if (player==null)
			throw new Exception("No estás identificado");
		if (player.getCurrentMatch()!=null)
			throw new Exception("Ya tienes una partida en marcha");
		Match match=Manager.get().joinGame(player, gameName);
		return match;
	}
		
	@RequestMapping("/games")
	public JSONArray games() throws Exception {
		return Manager.get().getGames();
	}
	
	@RequestMapping("/salir")
	public void salir(HttpSession session) throws Exception {
		Player player=(Player) session.getAttribute("player");
		session.invalidate();
		Manager.get().logout(player);
	}
	
	@RequestMapping(value="/requestToken", method=RequestMethod.POST)
	public Token requestToken(HttpSession session, @RequestParam(value="userName") String userName) throws Exception {
		Player player=Player.find(userName);
		Token token=null;
		if (player!=null)
			token=Manager.get().requestToken(player);
		return token;
	}
	
	@RequestMapping(value="/resetPassword", method=RequestMethod.POST)
	public Player resetPwd(@RequestParam(value="p") JSONObject jso) throws Exception {
		String userName=jso.getString("userName");
		String pwd1=jso.getString("pwd1");
		String pwd2=jso.getString("pwd2");
		int id=jso.getInt("token"); 
		if (!pwd1.equals(pwd2)) {
			Token.delete(id);
			throw new Exception("Error: las contraseñas no coinciden");
		}
		Player player=Manager.get().resetPwd(userName, pwd1, id);
		return player;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(HttpServletRequest req, Exception ex) {
		ModelAndView result = new ModelAndView();
		result.setViewName("respuesta");
		result.addObject("exception", ex);
		result.setStatus(HttpStatus.BAD_REQUEST);
	    return result;
	}
	
	@ExceptionHandler(LoginException.class)
	public ModelAndView handleLoginException(HttpServletRequest req, Exception ex) {
		ModelAndView result = new ModelAndView();
		result.setViewName("mensajeLogin");
		result.addObject("exception", ex);
		result.setStatus(HttpStatus.UNAUTHORIZED);
	    return result;
	}
}

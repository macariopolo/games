package edu.uclm.esi.banco.web.controllers;

import edu.uclm.esi.banco.model.*;

public class Manager {
	
	private Manager() {
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}


}

package edu.uclm.esi.banco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lanzadora {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Lanzadora.class, args);
	}

}

package edu.uclm.esi.banco.model;

public class TarjetaDebito extends Tarjeta {

	@Override
	public void ingresar(double importe) {
		this.cuenta.ingresar(importe, "Ingreso en cajero");
	}

	@Override
	public void retirar(double importe) throws Exception {
		this.cuenta.retirar(importe, "Retirada en cajero");
	}

}

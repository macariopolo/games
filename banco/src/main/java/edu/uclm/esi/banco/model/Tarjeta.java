package edu.uclm.esi.banco.model;

public abstract class Tarjeta {
	protected Cuenta cuenta;
	protected String numero;
	
	public Tarjeta() {
	}
	
	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}
	
	public abstract void ingresar(double importe);
	
	public abstract void retirar(double importe) throws Exception;
}

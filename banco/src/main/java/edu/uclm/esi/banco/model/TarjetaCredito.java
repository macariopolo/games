package edu.uclm.esi.banco.model;

public class TarjetaCredito extends Tarjeta {
	private double credito;
	private double creditoDisponible;

	public TarjetaCredito(double credito) {
		this.credito=credito;
		this.creditoDisponible=credito;
	}

	@Override
	public void ingresar(double importe) {
		this.creditoDisponible+=importe;
	}

	@Override
	public void retirar(double importe) throws Exception {
		double comision=importe*0.02;
		if (importe+comision>creditoDisponible)
			throw new Exception("El importe y la comisión superan el crédito disponible");
		creditoDisponible=creditoDisponible-importe-comision;
	}

}

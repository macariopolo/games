package edu.uclm.esi.banco.model;

import java.util.Vector;

public class Cuenta {
	protected Cliente titular;
	protected String numero;
	protected Vector<Movimiento> movimientos;
	
	public Cuenta() {
		this.movimientos=new Vector<>();
	}
	
	public void setTitular(Cliente titular) {
		this.titular = titular;
	}
	
	public void ingresar(double importe) {
		this.ingresar(importe, "Ingreso de efectivo");
	}

	public void ingresar(double importe, String concepto) {
		Movimiento m=new Movimiento(importe, concepto);
		this.movimientos.add(m);		
	}
	
	public void retirar(double importe) throws Exception {
		this.retirar(importe, "Retirada de efectivo");
	}
	
	public void retirar(double importe, String concepto) throws Exception {
		if (importe>this.getSaldo())
			throw new Exception("Disponible insufifiente");
		Movimiento m=new Movimiento(-importe, concepto);
		this.movimientos.add(m);		
	}

	private double getSaldo() {
		double r=0;
		for (Movimiento m : this.movimientos)
			r+=m.getImporte();
		return r;
	}
}

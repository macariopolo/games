package edu.uclm.esi.banco.model;

import java.util.Date;

public class Movimiento {

	private double importe;
	private String concepto;
	private Date fecha;

	public Movimiento(double importe, String concepto) {
		this.importe=importe;
		this.concepto=concepto;
		this.fecha=new Date();
	}

	public double getImporte() {
		return this.importe;
	}

}

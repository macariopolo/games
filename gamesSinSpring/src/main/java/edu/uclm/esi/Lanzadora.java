package edu.uclm.esi;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import edu.uclm.esi.games.dao.Broker;
import edu.uclm.esi.games.model.Player;


@SpringBootApplication
public class Lanzadora {

	public static void main(String[] args) throws Exception {
		long timeIni=System.currentTimeMillis();
		//insertarUsuarios(200);
		System.out.println("Tiempo empleado en creación de usuarios: " + (System.currentTimeMillis()-timeIni)/1000 + " segundos");
		
		SpringApplication.run(Lanzadora.class, args);
	}

	private static void insertarUsuarios(int n) throws Exception {
		Connection bd=Broker.get().getBd();
		PreparedStatement ps=bd.prepareStatement("Delete from Player");
		ps.executeUpdate();
		bd.close();
		
		Player.insert("ana@ana.com", "ana", "ana");
		Player.insert("pepe@pepe.com", "pepe", "pepe");
		
		for (int i=1; i<=n/2; i++) {
			Player.insert("ana" + i + "@ana.com", "ana" + i, "ana");
			Player.insert("pepe" + i + "@pepe.com", "pepe" + i, "pepe");
		}
	}

}

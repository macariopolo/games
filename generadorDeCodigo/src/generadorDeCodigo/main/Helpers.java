package generadorDeCodigo.main;

import java.util.Vector;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.DynamicEObjectImpl;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UseCase;

public class Helpers {
	public Vector<String> getValores(UseCase uc, String campo) {
		System.out.println("getValores-> " + uc.getName());
		Vector<String> result=new Vector<String>();
		EList<Stereotype> sstt = uc.getAppliedStereotypes();
		Stereotype st = sstt.get(0);
		EList<Property> attributes = st.getAllAttributes();
		try {
			EList valores = (EList) uc.getValue(st, attributes.get(1).getName());
			for (int i=0; i<valores.size(); i++) {
				DynamicEObjectImpl valor=(DynamicEObjectImpl) valores.get(i);
				EList<EStructuralFeature> features = valor.eClass().getEAllStructuralFeatures();
				EStructuralFeature featureBuscada=null;
				for (int j=0; j<features.size(); j++) {
					EStructuralFeature feature=features.get(j);
					if (feature.getName().equals(campo)) {
						featureBuscada=feature;
						break;
					}
				}
				if (featureBuscada!=null)
					result.add(valor.eGet(featureBuscada).toString());
				else
					result.add("[NO SE ENCUENTRA]");
			}
		} catch(Exception e) {
			
		}
		return result;
	}
	
	public String getListaDeCampos(UseCase uc, String campo) {
		System.out.println("getListaDeCampos-> " + uc.getName());
		String result="";
		EList<Stereotype> sstt = uc.getAppliedStereotypes();
		Stereotype st = sstt.get(0);
		EList<Property> attributes = st.getAllAttributes();
		try {
			EList valores = (EList) uc.getValue(st, attributes.get(1).getName());
			for (int i=0; i<valores.size(); i++) {
				DynamicEObjectImpl valor=(DynamicEObjectImpl) valores.get(i);
				EList<EStructuralFeature> features = valor.eClass().getEAllStructuralFeatures();
				EStructuralFeature featureBuscada=null;
				for (int j=0; j<features.size(); j++) {
					EStructuralFeature feature=features.get(j);
					if (feature.getName().equals(campo)) {
						featureBuscada=feature;
						break;
					}
				}
				if (featureBuscada!=null)
					result+=valor.eGet(featureBuscada).toString() + ", ";
				else
					result+="[NO SE ENCUENTRA], ";
			}
			if (result.length()>2)
				result=result.substring(0, result.length()-2);
		}
		catch (Exception e) {
		}
		return result;
	}
	
	public Vector<String> getColeccionDeValores(UseCase uc, String campo) {
		System.out.println("getColeccionDeValores-> " + uc.getName());
		Vector<String> result=new Vector<String>();
		EList<Stereotype> sstt = uc.getAppliedStereotypes();
		Stereotype st = sstt.get(0);
		EList<Property> attributes = st.getAllAttributes();
		try {
			EList valores = (EList) uc.getValue(st, attributes.get(1).getName());
			for (int i=0; i<valores.size(); i++) {
				String valor=valores.get(i).toString();
				result.add(valor);
			}
		} catch(Exception e) {
			System.out.println(e.toString());
		}
		return result;
	}
}

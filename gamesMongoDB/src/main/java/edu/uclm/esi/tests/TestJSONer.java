package edu.uclm.esi.tests;

import org.junit.Test;

import edu.uclm.esi.games.model.Match;
import edu.uclm.esi.games.model.Player;
import edu.uclm.esi.games.tictactoe.TictactoeGame;
import edu.uclm.esi.jsoner.JSONer;

public class TestJSONer {

	@Test
	public void test1() throws Exception {
		Player pepe=new Player();
		pepe.setEmail("pepe@pepe.com");
		pepe.setUserName("pepe");
		
		Player ana=new Player();
		ana.setEmail("ana@ana.com");
		ana.setUserName("ana");
		
		TictactoeGame ppt=new TictactoeGame();		
		Match match = ppt.getMatch(pepe);
		match=ppt.getMatch(ana);
		pepe.move(new Integer[] { 1, 1});
		
		match.setWinner(pepe);
		System.out.println(JSONer.toJSON(match));
	}

}

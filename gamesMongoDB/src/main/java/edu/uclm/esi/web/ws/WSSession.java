package edu.uclm.esi.web.ws;

import javax.websocket.Session;

import edu.uclm.esi.games.model.AbstractPlayer;

public class WSSession {
	private Session session;
	private AbstractPlayer player;

	public WSSession(Session session, AbstractPlayer player) {
		this.session=session;
		this.player=player;
	}

	public Session getSession() {
		return this.session;
	}
	
	public String getId() {
		return this.session.getId();
	}

	public void setPlayer(AbstractPlayer player) {
		this.player=player;
	}

	public AbstractPlayer getPlayer() {
		return this.player;
	}
}
package edu.uclm.esi.games.model;

import org.bson.types.ObjectId;
import org.json.JSONObject;

import edu.uclm.esi.jsoner.JSONExclude;
import edu.uclm.esi.mongolabels.labels.Bsonable;

public abstract class Board {
	@Bsonable
	protected ObjectId _id;
	@Bsonable
	protected int timesPlayed;
	@Bsonable
	protected int bestTime;
	@Bsonable
	protected int bestMovements;
	@JSONExclude
	protected Match match;
	
	public Board() {
	}
	
	public Board(Match match) {
		this.match=match;
		this.bestTime=Integer.MAX_VALUE;
		this.bestMovements=Integer.MAX_VALUE;
	}

	public abstract void move(AbstractPlayer player, Integer[] coordinates) throws Exception;
	public abstract AbstractPlayer getWinner();
	public abstract boolean end();
	public abstract String getContent();

	public JSONObject toJSON() throws Exception {
		throw new Exception("toJSON not implemented in " + this.getClass().getSimpleName());
	}
	
	public ObjectId get_id() {
		return _id;
	}
	
	public void setMatch(Match match) {
		this.match = match;
	}

	public void increaseTimesPlayed() {
		this.timesPlayed++;
	}
	
	public int getBestMovements() {
		return bestMovements;
	}
	
	public void setBestMovements(int bestMovements) {
		this.bestMovements = bestMovements;
	}
	
	public int getBestTime() {
		return bestTime;
	}
	
	public void setBestTime(int bestTime) {
		this.bestTime = bestTime;
	}

	public abstract boolean draw();
}

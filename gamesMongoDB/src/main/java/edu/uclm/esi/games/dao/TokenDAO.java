package edu.uclm.esi.games.dao;

import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonString;

import edu.uclm.esi.games.model.Token;
import edu.uclm.esi.mongolabels.dao.MongoBroker;

public class TokenDAO {

	public static void insert(Token token) throws Exception {
		MongoBroker.get().insert(token);
	}

	public static void find(String userName, int idToken) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("userName", new BsonString(userName));
		criterion.append("id", new BsonInt32(idToken));
		Token token=(Token) MongoBroker.get().loadOne("Token", criterion);
		if (token.getValidTime()<System.currentTimeMillis())
			throw new Exception("Token expirado o token no existe");
	}

	public static void delete(int idToken) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("id", new BsonInt32(idToken));
		MongoBroker.get().delete("Token", criterion);
	}
}

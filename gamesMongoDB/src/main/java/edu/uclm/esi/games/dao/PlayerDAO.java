package edu.uclm.esi.games.dao;

import org.bson.BsonDocument;
import org.bson.BsonString;

import edu.uclm.esi.games.model.AbstractPlayer;
import edu.uclm.esi.games.model.Player;
import edu.uclm.esi.mongolabels.dao.MongoBroker;

public class PlayerDAO {

	public static void insert(Player player) throws Exception {
		MongoBroker.get().insert(player);
	}

	public static Player identify(String userName, String pwd) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("userName", new BsonString(userName));
		criterion.append("pwd", new BsonString(pwd));
		return (Player) MongoBroker.get().loadOne(Player.class, criterion);
	}

	public static Player find(String userName) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("userName", new BsonString(userName));
		return (Player) MongoBroker.get().loadOne(Player.class, criterion);
	}

	public static Player updatePwd(String userName, String pwd) throws Exception {
		throw new Exception("Not implemented");
	}

	public static void update(Player player) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.put("userName", new BsonString(player.getUserName()));
		String collectionName=AbstractPlayer.class.getSimpleName();
		MongoBroker.get().delete(collectionName, criterion);
		MongoBroker.get().insert(player);
	}

}

package edu.uclm.esi.games.dao;

import org.bson.BsonDocument;
import org.bson.BsonString;

import edu.uclm.esi.games.model.AbstractPlayer;
import edu.uclm.esi.games.model.SimplePlayer;
import edu.uclm.esi.mongolabels.dao.MongoBroker;

public class SimplePlayerDAO {

	public static void insert(SimplePlayer player) throws Exception {
		MongoBroker.get().insert(player);
	}

	public static SimplePlayer identify(String userName) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("userName", new BsonString(userName));
		return (SimplePlayer) MongoBroker.get().loadOne(SimplePlayer.class, criterion);
	}

	public static void changeUserName(String oldName, String newName) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("userName", new BsonString(newName));
		SimplePlayer user=(SimplePlayer) MongoBroker.get().loadOne(SimplePlayer.class, criterion);
		if (user!=null)
			throw new Exception("The user name" + newName + " already exists. Choose another");
		user=new SimplePlayer();
		user=SimplePlayer.insert(newName);
		delete(oldName);		
	}

	public static void delete(String userName) {
		BsonDocument criterion=new BsonDocument();
		criterion.append("userName", new BsonString(userName));
		MongoBroker.get().delete(SimplePlayer.class, criterion);
	}

	public static void update(SimplePlayer simplePlayer) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.put("userName", new BsonString(simplePlayer.getUserName()));
		String collectionName=AbstractPlayer.class.getSimpleName();
		MongoBroker.get().delete(collectionName, criterion);
		MongoBroker.get().insert(simplePlayer);
	}

}

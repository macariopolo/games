package edu.uclm.esi.games.model;

import java.util.Random;

import edu.uclm.esi.games.dao.TokenDAO;

public class Token {
	private Integer id;
	private Long validTime;
	private String userName; 
	
	public Token(String userName) {
		this.userName=userName;
		this.id=Math.abs(new Random().nextInt());
		this.validTime=System.currentTimeMillis() + 10*60*1000;
	}
	
	public void insert() throws Exception {
		TokenDAO.insert(this);
	}
	
	public int getId() {
		return id;
	}

	public String getUserName() {
		return userName;
	}
	
	public long getValidTime() {
		return validTime;
	}

	public static void load(String userName, int idToken) throws Exception {
		TokenDAO.find(userName, idToken);
	}

	public static void delete(int id) throws Exception {
		TokenDAO.delete(id);
	}
}

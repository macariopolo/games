package edu.uclm.esi.games.kuar;

import java.util.Random;
import java.util.Vector;

import org.bson.types.ObjectId;

import edu.uclm.esi.games.dao.GameDAO;
import edu.uclm.esi.games.model.Board;
import edu.uclm.esi.games.model.Game;
import edu.uclm.esi.games.model.Match;
import edu.uclm.esi.mongolabels.dao.MongoBroker;

public class KuarGame extends Game {
	private int rows;

	public KuarGame(int rows) {
		this.rows=rows;
	}

	@Override
	public String getName() {
		return "Kuar " + rows + "x" + rows;
	}

	@Override
	protected Match createMatch() {
		Board board=getRandomBoard(false);
		Match match = new KuarMatch(this);
		match.setBoard(board);
		return match;
	}
	
	@Override
	public Board findBoard(ObjectId idBoard) throws Exception {
		return GameDAO.findBoard(KuarBoard.class, idBoard);
	}

	@Override
	public Board getRandomBoard(boolean testingMode) {
	    String contenido=null;
	    Board board=null;
	    
	    if (testingMode) {
	    		try {
	    			String id="5c20a4ec4034f8ea0c53dc96";
	    			if (rows==4)
	    				id="5c20ad394034f8ea695cd5d0";
	    			board=GameDAO.findBoard(KuarBoard.class, new ObjectId(id));
				if (board!=null)
					contenido=board.getContent();
				return board;
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    
		if (new Random().nextBoolean()) {
			try {
				board=GameDAO.findBoard(KuarBoard.class, rows);
				if (board!=null)
					contenido=board.getContent();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		
		if (contenido==null) {	
			double percentage=0.8;
		    int total=(int) ((rows*rows)*percentage);
		    Vector<Pair> selCoords=new Vector<Pair>();
		    int n=0;
		    Random dado=new Random();
		    int dadoRow, dadoCol;
		    int[][] squares=new int[rows][rows];
		    while (n<total) {
		        Pair pair=new Pair();
		        do {
		            dadoRow = dado.nextInt(rows);
		            dadoCol = dado.nextInt(rows);
		            pair.row=dadoRow; pair.col=dadoCol;
		        } while (selCoords.contains(pair));
		        selCoords.add(pair);
		        n++;
		        squares[dadoRow][dadoCol]=n;
		    }
		    contenido="";
		    for (int i=0; i<rows; i++)
			    	for (int j=0; j<rows; j++)
			    		contenido+=squares[i][j] + ",";
		    
		    contenido=contenido.substring(0, contenido.length()-1);
		    ObjectId idBoard=new ObjectId();
			board=new KuarBoard(idBoard, rows, contenido);
			try {
				MongoBroker.get().insert(board);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return board;
	}
}

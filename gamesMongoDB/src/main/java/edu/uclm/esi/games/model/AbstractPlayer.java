package edu.uclm.esi.games.model;

import javax.servlet.http.HttpSession;

import edu.uclm.esi.jsoner.JSONExclude;
import edu.uclm.esi.mongolabels.labels.Bsonable;

public abstract class AbstractPlayer {
	@Bsonable
	protected String userName;
	@JSONExclude
	protected Match currentMatch;
	@Bsonable
	protected int victories;
	@Bsonable
	protected int defeats;
	@Bsonable
	protected int withdrawals;
	@JSONExclude
	protected HttpSession httpSession;
		
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public void setCurrentMatch(Match match) {
		this.currentMatch=match;
	}
	
	public Match getCurrentMatch() {
		return currentMatch;
	}
	
	public Match move(Integer[] coordinates) throws Exception {
		return this.currentMatch.move(this, coordinates);
	}

	public void increaseVictoriesDefeatsWithdrawals(int victories, int defeats, int withdrawals) throws Exception {
		this.victories+=victories;
		this.defeats+=defeats;
		this.withdrawals+=withdrawals;
		update();
	}

	protected abstract void update() throws Exception;

	public int getVictories() {
		return victories;
	}
	
	public int getDefeats() {
		return defeats;
	}
	
	public int getWithdrawals() {
		return withdrawals;
	}

	public void invalidateSession() {
		this.httpSession.invalidate();
	}
	
	public void setHttpSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}
}

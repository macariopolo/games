package edu.uclm.esi.games.dao;

import java.util.Random;

import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonObjectId;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.games.model.Board;
import edu.uclm.esi.mongolabels.dao.MongoBroker;

public class GameDAO {

	public static Board findBoard(Class<?> boardClazz, int rows) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("rows", new BsonInt32(rows));
		
		JSONArray boards = MongoBroker.get().loadAll(boardClazz, criterion, "_id");
		if (boards.length()==0)
			return null;
		int dado=new Random().nextInt(boards.length());
		JSONObject jso=boards.getJSONObject(dado);
		BsonObjectId _id=new BsonObjectId(new ObjectId(jso.getJSONObject("_id").getString("$oid")));
		criterion.clear();
		criterion.append("_id", _id);
		return (Board) MongoBroker.get().loadOne(boardClazz, criterion);
	}

	public static Board findBoard(Class<?> boardClazz, ObjectId idBoard) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.append("_id", new BsonObjectId(idBoard));
		return (Board) MongoBroker.get().loadOne(boardClazz, criterion);
	}

	public static void update(Board board) throws Exception {
		BsonDocument criterion=new BsonDocument();
		criterion.put("_id", new BsonObjectId(board.get_id()));
		MongoBroker.get().delete(Board.class, criterion);
		MongoBroker.get().insert(board);
	}

}

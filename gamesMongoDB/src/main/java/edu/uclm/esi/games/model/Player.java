package edu.uclm.esi.games.model;

import edu.uclm.esi.games.dao.PlayerDAO;
import edu.uclm.esi.jsoner.JSONExclude;
import edu.uclm.esi.mongolabels.labels.Bsonable;

public class Player extends AbstractPlayer {
	@Bsonable
	private String email;
	@Bsonable @JSONExclude
	private String pwd;
	
	public Player() {
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public static Player insert(String email, String userName, String pwd) throws Exception {
		Player player=new Player();
		player.setEmail(email);
		player.setUserName(userName);
		player.pwd=pwd;
		PlayerDAO.insert(player);
		return player;
	}

	public static Player identify(String userName, String pwd) throws Exception {
		return PlayerDAO.identify(userName, pwd);
	}

	public static Player find(String userName) throws Exception {
		return PlayerDAO.find(userName);
	}

	public static Player updatePwd(String userName, String pwd) throws Exception {
		return PlayerDAO.updatePwd(userName, pwd);
	}

	@Override
	protected void update() throws Exception {
		PlayerDAO.update(this);
	}

}

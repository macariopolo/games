package edu.uclm.esi.mongolabels.dao;

import org.bson.types.ObjectId;

import edu.uclm.esi.mongolabels.labels.Bsonable;

public class Host {
	@Bsonable
	private ObjectId _id;
	@Bsonable
	private String name;
	@Bsonable
	private String avdPath;
	@Bsonable
	private String sdkPath;
	
	public Host() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvdPath() {
		avdPath=avdPath.replace("\\", "/");
		if (!avdPath.endsWith("/"))
			avdPath+="/";
		return avdPath;
	}

	public void setAvdPath(String avdPath) {
		this.avdPath = avdPath;
	}

	public ObjectId get_id() {
		return _id;
	}
	
	public String getSdkPath() {
		sdkPath=sdkPath.replace("\\", "/");
		if (!sdkPath.endsWith("/"))
			sdkPath=sdkPath+"/";
		return sdkPath;
	}
	
	public void setSdkPath(String sdkPath) {
		this.sdkPath = sdkPath;
	}
}

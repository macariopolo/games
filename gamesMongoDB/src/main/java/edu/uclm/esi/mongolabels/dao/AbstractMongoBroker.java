package edu.uclm.esi.mongolabels.dao;

import org.bson.BsonDocument;
import org.bson.BsonObjectId;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public abstract class AbstractMongoBroker {
	public abstract BsonObjectId insert(Object object) throws Exception;
	public abstract BsonObjectId insertBson(Class<?> clazz, BsonDocument bso) throws Exception;
	
	public BsonObjectId insertBson(MongoDatabase db, Class<?> clazz, BsonDocument bso) throws Exception {
		Class<?> collectionClass=getCollectionClass(clazz);
		MongoCollection<BsonDocument> collection=db.getCollection(collectionClass.getSimpleName(), BsonDocument.class);
		collection.insertOne(bso);
		return bso.getObjectId("_id");
	}
	
	public BsonObjectId insert(MongoDatabase db, Object object) throws Exception {
		BsonDocument bso=Object2Bson.getBsonDocument(object);
		Class<?> collectionClass=getCollectionClass(object.getClass());
		MongoCollection<BsonDocument> collection=db.getCollection(collectionClass.getSimpleName(), BsonDocument.class);
		collection.insertOne(bso);
		return bso.getObjectId("_id");
	}
	
	protected Class<?> getCollectionClass(Class<?> clazz) {
		Class<?> collectionClass=clazz;
		while (collectionClass.getSuperclass()!=java.lang.Object.class)
			collectionClass=collectionClass.getSuperclass();
		return collectionClass;
	}


}

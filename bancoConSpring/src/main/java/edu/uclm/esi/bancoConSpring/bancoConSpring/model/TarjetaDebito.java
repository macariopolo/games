package edu.uclm.esi.bancoConSpring.bancoConSpring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TarjetaDebito extends Tarjeta {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	protected int idTarjetaDebito;

	@Override
	public void ingresar(double importe) {
		this.cuenta.ingresar(importe, "Ingreso en cajero");
	}

	@Override
	public void retirar(double importe) throws Exception {
		this.cuenta.retirar(importe, "Retirada en cajero");
	}

}

package edu.uclm.esi.bancoConSpring.bancoConSpring.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Cuenta {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	protected int id;
	@ManyToOne
	protected Cliente titular;
	protected String numero;
	
	@OneToMany
	protected List<Movimiento> movimientos;
	
	public Cuenta() {
		this.movimientos=new ArrayList<>();
	}
	
	public void setTitular(Cliente titular) {
		this.titular = titular;
	}
	
	public void ingresar(double importe) {
		this.ingresar(importe, "Ingreso de efectivo");
	}

	public void ingresar(double importe, String concepto) {
		Movimiento m=new Movimiento(importe, concepto);
		this.movimientos.add(m);		
	}
	
	public void retirar(double importe) throws Exception {
		this.retirar(importe, "Retirada de efectivo");
	}
	
	public void retirar(double importe, String concepto) throws Exception {
		if (importe>this.getSaldo())
			throw new Exception("Disponible insufifiente");
		Movimiento m=new Movimiento(-importe, concepto);
		this.movimientos.add(m);		
	}

	private double getSaldo() {
		double r=0;
		for (Movimiento m : this.movimientos)
			r+=m.getImporte();
		return r;
	}
}

package edu.uclm.esi.bancoConSpring.bancoConSpring.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Movimiento {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	protected int id;

	private double importe;
	private String concepto;
	private Date fecha;

	public Movimiento(double importe, String concepto) {
		this.importe=importe;
		this.concepto=concepto;
		this.fecha=new Date();
	}

	public double getImporte() {
		return this.importe;
	}

}

package edu.uclm.esi.bancoConSpring.bancoConSpring.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

// @MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS) 
public abstract class Tarjeta {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	protected int id;
	@ManyToOne
	protected Cuenta cuenta;
	protected String numero;
	
	public Tarjeta() {
	}
	
	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}
	
	public abstract void ingresar(double importe);
	
	public abstract void retirar(double importe) throws Exception;
}

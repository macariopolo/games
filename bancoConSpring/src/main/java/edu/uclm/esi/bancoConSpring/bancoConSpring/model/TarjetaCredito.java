package edu.uclm.esi.bancoConSpring.bancoConSpring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TarjetaCredito extends Tarjeta {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	protected int idTarjetaCredito;

	private double credito;
	private double creditoDisponible;

	public TarjetaCredito(double credito) {
		this.credito=credito;
		this.creditoDisponible=credito;
	}

	@Override
	public void ingresar(double importe) {
		this.creditoDisponible+=importe;
	}

	@Override
	public void retirar(double importe) throws Exception {
		double comision=importe*0.02;
		if (importe+comision>creditoDisponible)
			throw new Exception("El importe y la comisión superan el crédito disponible");
		creditoDisponible=creditoDisponible-importe-comision;
	}

}
